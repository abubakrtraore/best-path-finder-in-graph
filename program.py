from geopy.geocoders import Nominatim 
import folium 
import webbrowser
from geopy.distance import vincenty 
import math
geolocator = Nominatim()


data = [ ["Etat","Capitale"],["Alaska","Juneau"],["Oregon","Salem"],
["Californie","Sacramento"],["Arizona","Phoenix"],["Montana","Helena"], ["Alabama","Montgomery"],["Arkansas","Little Rock"],
["Caroline du Nord","Raleigh"],["Colorado","Denver"], ["Dakota du Nord","Bismarck"],["Connecticut","Hartford"],
["Dakota du Sud","Pierre"],["Delaware","Dover"],["Floride","Tallahassee"],
["Atlanta","Géorgie"],["Idaho","Boise"],["Illinois","Springfield"],
["Indiana","Indianapolis"],["Iowa","Des Moines"],["Kansas","Topeka"],
["Louisiane","Bâton-Rouge"],["Maryland","Annapolis"],
["Massachusetts","Boston"],["Michigan","Lansing"],["Mississippi","Jackson"], ["Missouri","Jefferson City"],["Nebraska","Lincoln"],
["Nevada","Carson City"],["New Hampshire","Concord"],
["New Jersey","Trenton"],["Nouveau-Mexique","Santa Fe"],
["New  York","Albany"],["Ohio","Columbus"],["Oklahoma","Oklahoma  City"], ["Olympia","Washington"],["Pennsylvanie","Harrisburg"],
["Rhode Island","Providence"],["Tennessee","Nashville"],["Texas","Austin"],
["Utah","Salt Lake City"],["Vermont","Montpelier"],["Virginie","Richmond"], ["Virginie-Occidentale","Charleston"],["Wisconsin","Madison"], ["Wyoming","Cheyenne"],["Caroline du Sud","Columbia"], ["Kentucky","Frankfort"],["Maine","Augusta"],["Minnesota","Saint Paul"]]


for i in enumerate(data): 
    print (i)


etats = []
for i in range(1,len(data)): 
    etats+=[data[i][0]]


Coord=[]
for i in etats:
    location = geolocator.geocode(i)
    Coord += [[location.latitude, location.longitude]]



my_map = folium.Map(location=[38.27312,-98.5821871], tiles='Stamen Toner',zoom_start=5) 
k=1
for each in Coord: 
    folium.RegularPolygonMarker(location=each,popup=data[k][0], fill_color='blue', number_of_sides=10, radius=10).add_to(my_map)
    k+=1


liaison =[["Alaska","Olympia","Montana","Dakota du Nord"], ["Olympia","Alaska","Oregon","Idaho"],
["Montana","Idaho","Wyoming","Dakota du Nord","Alaska"],
["Wyoming","Idaho","Montana","Dakota du Sud","Nebraska","Colorado","Utah"], ["Dakota du Nord","Alaska","Minnesota","Dakota du Sud","Montana"],
["Dakota du Sud","Minnesota","Dakota du Nord","Nebraska","Iowa","Montana","Wyoming"], ["Minnesota","Wisconsin","Dakota du Nord","Dakota du Sud","Iowa"], ["Wisconsin","Minnesota","Iowa","Illinois"],
["Michigan","Indiana","Ohio"],
["Indiana","Ohio","Illinois","Kentucky"],
["Illinois","Missouri","Indiana","Wisconsin","Iowa"],
["Missouri","Kansas","Arkansas","Illinois","Iowa"], ["Iowa","Illinois","Wisconsin","Minnesota","Dakota du Sud","Nebraska","Missouri"], ["Kansas","Missouri","Nebraska","Colorado","Oklahoma"],
["Colorado","Kansas","Nebraska","Wyoming","Utah","Nouveau-Mexique"],
["Utah","Wyoming","Colorado","Idaho","Nevada","Arizona"],
["Idaho","Wyoming","Montana","Oregon","Nevada","Utah"],
["Oregon","Idaho","Olympia","Californie","Nevada"],
["Nevada","Utah","Idaho","Oregon","Californie","Arizona"],
["Nouveau-Mexique","Texas","Colorado","Arizona"], ["Texas","Louisiane","Arkansas","Oklahoma","Nouveau-Mexique"], ["Oklahoma","Arkansas","Kansas","Texas"], ["Louisiane","Mississippi","Arkansas","Texas"], ["Mississippi","Alabama","Arkansas","Louisiane"], ["Arkansas","Missouri","Tennessee","Oklahoma","Texas","Louisiane","Mississippi"], ["Alabama","Atlanta","Tennessee","Mississippi","Floride"], ["Tennessee","Kentucky","Caroline du Nord","Virginie","Arkansas","Alabama","Atlanta"], ["Atlanta","Caroline du Sud","Tennessee","Alabama","Floride"], ["Kentucky","Virginie","Virginie-Occidentale","Ohio","Indiana","Tennessee"], ["Ohio","Pennsylvanie","Michigan","Indiana","Kentucky","Virginie-Occidentale"], ["Caroline du Sud","Caroline du Nord","Atlanta"],
["Caroline du Nord","Virginie","Tennessee","Caroline du Sud"], ["Delaware","Maryland","New  Jersey"],
["Pennsylvanie","New  Jersey","Maryland","New  York"], ["New  York","Pennsylvanie","New  Jersey","Connecticut"], ["New  Jersey","New  York","Pennsylvanie","Delaware"], ["Rhode Island","Connecticut","Massachusetts"],
["Connecticut","Rhode  Island","New  York","Massachusetts"], ["Massachusetts","Connecticut","New  Hampshire","Rhode  Island"],
["New Hampshire","Massachusetts","Vermont","Maine"], ["Vermont","New  Hampshire","Maine"],
["Maine","Vermont","New  Hampshire"],
["Floride","Atlanta","Alabama"],
["Virginie","Tennessee","Virginie-Occidentale","Caroline du Nord","Kentucky"], ["Virginie-Occidentale","Virginie","Kentucky","Ohio"], ["Maryland","Delaware","Pennsylvanie"], ["Californie","Nevada","Oregon","Arizona"],
["Arizona","Utah","Nouveau-Mexique","Nevada","Californie"],
["Nebraska","Wyoming","Dakota du Sud","Kansas","Colorado","Iowa"]]


from geopy.geocoders import Nominatim 
geolocator = Nominatim()
for i in liaison:
    Coordonnees=[]
    location = geolocator.geocode(i[0])
    Coordonnees += [[location.latitude,location.longitude]]
    for k in range(1,len(i)):
        location = geolocator.geocode(i[k])
        Coordonnees += [[location.latitude,location.longitude]] 
        folium.PolyLine(Coordonnees, color="green", weight=2.5, opacity=1).add_to(my_map) 
        Coordonnees = [Coordonnees[0]]
my_map.save('/home/big/Jupyter Workspace/macarte1.html')
url = '/home/big/Jupyter Workspace/macarte1.html'
webbrowser.open_new_tab(url)


def way(B,poids):
    if  B  not  in  etats:  
        return  ("Aucune  correspondance  dans  le  modèle  actuel")
    else:
        way = [B]
        for j in poids:
            for k in poids:
                if k[0] == B:
                    way+=[k[2]] 
                    B = k[2]
        return (way)


#Calcul de tous les chemins optimaux à partir d’une ville donnée A

def optimum(A):
    #Récupérer  la  liste  de  l’état  donné  en  paramètre  et  des  états  qui  lui  sont  liés. 
    if  (A  not  in  etats):  
        return  ("Aucune  correspondance  dans  le  modèle  actuel") 
    else:
        for i in liaison:
            if  i[0] == A:
                link = i

        #Création  de  l’output  final
    optimale = [] 
    S = []
    for i in range(0,len(etats)): 
        S += [etats[i]];
    print("\n \n Voici S au tout début \n de longueur", len(S)) 
    print(S)
    #On  enlève  directement  la  ville  A  de  l’ensemble  S.
    S.remove(A)
    #Création du vecteur poids
    poids=[]
    for i in range(0,len(etats)): 
        poids+=[[etats[i],math.inf,"none"]]
    #Le sommet A prend un poids nul
    for j in range(0,len(etats)):
        if poids[j][0] == A: 
            poids[j][1] = 0
    print("\n \n voici le vecteur poids juste avant le début de la boucle \n") 
    print (poids)
    while(len(S)>1):
        location_1 = geolocator.geocode(A)
        
        #Stocker temporairement le vecteur poids
        weight = [0]*len(etats) #donne une liste de 0 de taille 49
        for  i  in  range(0,  len(poids)): 
            weight[i]=poids[i]
    
        #suppression  de  l’ancien  contenu  de  a  pour  en  mettre  un  autre #del(poids[:])
        for j in link:
            for k in range(0,len(etats)):
                if weight[k][0] == A:
                    for i in range(0,len(etats)):
                        if  (j == weight[i][0]):
                            location_2 = geolocator.geocode(j) #vérifier  si  le  poids  reste  inchangé  
                            if (min(weight[i][1], weight[k][1] + vincenty((location_1.latitude, location_1.longitude), (location_2.latitude,location_2.longitude)).miles)==weight[i][1]):
                                poids[i] = [j,min(weight[i][1], weight[k][1] + vincenty((location_1.latitude, location_1.longitude), (location_2.latitude, location_2.longitude)).miles), weight[i][2]]
                            else:
                                poids[i] = [j,min(weight[i][1], weight[k][1] + vincenty((location_1.latitude, location_1.longitude), (location_2.latitude, location_2.longitude)).miles),A]
                                
        #Selection  du  min  et  relancement  de  l’algo.  On  évite  le  0  en  selectionnant  les
        positive_weight = [] 
        state_pos = []
        for k in poids:
            if (k[1]>0 and k[0] in S): #ne pas prendre les poids des états déjà sélectionnés
                positive_weight+=[k[1]] 
                state_pos += [k[0]]
        
        min_weight = min(positive_weight)
        for i in range(0,len(positive_weight)):
            if positive_weight[i] == min_weight: 
                select  =  [state_pos[i],min_weight];

        #retrace le chemin des prédecesseurs
        for k in poids:
            if k[0] == select[0]:
                if  (k[2]  not  in  optimale):  #eviter  de  mettre  2  fois  le  même  état  dans  l’output
                    optimale += [k[2]]
                    print ("\n \n Etat actuel du output \n") 
                    print(optimale)
                    print("\n \n voici l'état de valuation minimale \n") 
                    print (select[0])
        #On remet dans "link" les états liés au sommet sélectionné
        for i in range(0,len(liaison)):
            if liaison[i][0] == select[0]: 
                link = liaison[i];
                print("\n \n voici les états liés à notre état sélectionné \n") 
                print (link)
        #On supprime les précédents sélectionnés
        if A in link:
            link.remove(A)
        print("\n \n voici les états liés au selectionné après avoir effacer le précedent \n") 
        print (link)
        
        #Changer la valeur de A pour relancer la boucle
        A = select[0]
        #On enlève la ville sélectionnée de S.
        S.remove(A)
    return (poids)




#Exemple
poids = optimum("Californie") 
best_way = way("Massachusetts",poids)
print(best_way)

best_way.reverse() 
best_way.remove("none") 
print(best_way)


Coord2=[]
location = geolocator.geocode(best_way[0])
Coord2 += [[location.latitude, location.longitude]] 
location  =  geolocator.geocode(best_way[len(best_way)-1]) 
Coord2 += [[location.latitude, location.longitude]]

folium.Marker(Coord2[0]).add_to(my_map); folium.Marker(Coord2[1]).add_to(my_map); Coordonnees = []
for  i  in  range(0,len(best_way)-1):
    location = geolocator.geocode(best_way[i])
    Coordonnees += [[location.latitude,location.longitude]] 
    location = geolocator.geocode(best_way[i+1]) 
    Coordonnees += [[location.latitude,location.longitude]]
    folium.PolyLine(Coordonnees,  color="red",  weight=2,  opacity=1).add_to(my_map) 
    Coordonnees = []
my_map.save('/home/big/Jupyter Workspace/macarte2.html')
url = '/home/big/Jupyter Workspace/macarte2.html' 
webbrowser.open_new_tab(url)


def distance(B,poids):
    if  B  not  in  etats:  
        return  ("Aucune  correspondance  dans  le  modèle  actuel") 
    else:
        for i in poids:
            if i[0] == B:
                return  (i[1])

dist = distance("Massachusetts", poids)
print (dist)


def  best_path(liste):
    
    my_map2 = folium.Map(location=[38.27312,-98.5821871], tiles='Stamen Toner',zoom_start=5)
    #Regénération du réseau
    Coord = []
    for i in etats:
        location = geolocator.geocode(i)
        Coord += [[location.latitude, location.longitude]]
    k=0
    for each in Coord:
        folium.RegularPolygonMarker(location=each,  popup=etats[k],fill_color='blue', number_of_sides=10,  radius=10).add_to(my_map2);
        k+=1
    for i in liaison:
        Coordonnees=[]
        location = geolocator.geocode(i[0])
        Coordonnees += [[location.latitude,location.longitude]]
        for k in range(1,len(i)):
            location = geolocator.geocode(i[k])
            Coordonnees += [[location.latitude,location.longitude]] 
            folium.PolyLine(Coordonnees, color="green", weight=2.5, opacity=1).add_to(my_map2) 
            Coordonnees = [Coordonnees[0]]
    #Traitement de la liste
    for k in liste:
        if  k  not  in  etats:  
            return  ("Un  des  états  donnés  ne  correspond  pas  au  modèle")

    for  j  in  range(0,len(liste)-1):
        poids = optimum(liste[j]) 
        best_way = way(liste[j+1],poids) 
        best_way.reverse() 
        best_way.remove("none") 
        Coordonnees = []
        for  i  in  range(0,len(best_way)-1):
            location = geolocator.geocode(best_way[i])
            Coordonnees += [[location.latitude,location.longitude]] 
            location = geolocator.geocode(best_way[i+1]) 
            Coordonnees += [[location.latitude,location.longitude]]
            folium.PolyLine(Coordonnees,  color="red",  weight=2,  opacity=1).add_to(my_map2) 
            Coordonnees = []
    for k in liste:
        location = geolocator.geocode(k) 
        folium.Marker([location.latitude,location.longitude]).add_to(my_map2);
    my_map2.save('/home/big/Jupyter Workspace/macarte3.html') 
    url = '/home/big/Jupyter Workspace/macarte3.html'

liste = ["Arizona","Dakota du Nord","Mississippi","Virginie-Occidentale"]
best_path(liste)

etats = ["Kayes","Koulikoro","Sikasso","Ségou","Mopti","Tombouctou","Gao","Kidal"]
liaison=[["Kayes","Koulikoro"], ["Koulikoro","Ségou","Sikasso"],
        ["Sikasso","Koulikoro","Ségou"],
        ["Ségou","Koulikoro","Sikasso","Mopti"],
        ["Mopti","Ségou","Tombouctou"],
        ["Tombouctou","Mopti","Gao","Kidal"],
        ["Gao","Kidal","Tombouctou"],
        ["Kidal","Tombouctou","Gao"]]

Coord=[]
for i in etats:
    location = geolocator.geocode(i)
    Coord += [[location.latitude, location.longitude]]

my_map3  =  folium.Map(location=[14.4879631,-4.1905985],zoom_start=6) 
k=0
for each in Coord:
    folium.Marker(location=each,  popup=etats[k]).add_to(my_map3) 
    k+=1
my_map3.save('/home/big/Jupyter Workspace/macarte.html') 
url = '/home/big/Jupyter Workspace/macarte.html' 
webbrowser.open_new_tab(url)

for i in liaison:
    Coordonnees=[]
    location = geolocator.geocode(i[0])
    Coordonnees += [[location.latitude,location.longitude]]
    for k in range(1,len(i)):
        location = geolocator.geocode(i[k])
        Coordonnees += [[location.latitude,location.longitude]] 
        folium.PolyLine(Coordonnees,  color="blue",  weight=3,  opacity=1).add_to(my_map3) 
        Coordonnees = [Coordonnees[0]]
my_map3.save('/home/big/Jupyter Workspace/macarte4.html') 
url = '/home/big/Jupyter Workspace/macarte4.html' 
webbrowser.open_new_tab(url)

best_way.reverse() 
best_way.remove("none") 
poids = optimum("Kayes")
best_way2 = way("Kidal",poids) 
best_way2



Coord2=[]
location = geolocator.geocode(best_way[0])
Coord2 += [[location.latitude, location.longitude]] 
location  =  geolocator.geocode(best_way[len(best_way)-1]) 
Coord2 += [[location.latitude, location.longitude]]

folium.Marker(Coord2[0]).add_to(my_map3) 
folium.Marker(Coord2[1]).add_to(my_map3) 
Coordonnees = []
for  i  in  range(0,len(best_way)-1):
    location = geolocator.geocode(best_way[i])
    Coordonnees += [[location.latitude,location.longitude]] 
    location = geolocator.geocode(best_way[i+1]) 
    Coordonnees += [[location.latitude,location.longitude]]
    folium.PolyLine(Coordonnees,  color="red",  weight=2,  opacity=1).add_to(my_map3) 
    Coordonnees = []
my_map3.save('/home/big/Jupyter Workspace/macarte5.html') 
url = '/home/big/Jupyter Workspace/macarte5.html' 
webbrowser.open_new_tab(url)
